{ config, pkgs, ... }:

let
  catppuccin-fish = pkgs.fetchFromGitHub {
    owner = "catppuccin";
    repo = "fish";
    rev = "0ce27b518e8ead555dec34dd8be3df5bd75cff8e";
    hash = "sha256-Dc/zdxfzAUM5NX8PxzfljRbYvO9f9syuLO8yBr+R3qg=";
  };
in
{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "daniel";
  home.homeDirectory = "/home/daniel";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.05"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs;[
    neovim
    fzf
    xclip
    fd
    ripgrep
    go
    lazygit
    direnv
    # # Adds the 'hello' command to your environment. It prints a friendly
    # # "Hello, world!" when run.
    # pkgs.hello

    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };

  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/daniel/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  home.sessionVariables = { };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  programs.fish = {
    enable = true;
    shellAliases = {
      hanuman = "cd /home/daniel/Eggbedded/envitech_hanuman/";
    };
    shellInit = ''
      direnv hook fish | source
        # chmod +w -R ~/.local/share/omf
    '';

    interactiveShellInit = ''
      set fish_greeting "" # Disable greeting (optional)
      # fish_config theme save "Catppuccin Macchiato"
    '';
    plugins = with pkgs.fishPlugins; [
      { name = "hydro"; src = hydro.src; }
    ];
  };

  # Doesnt really work, TODO:
  # xdg.configFile."fish/themes/Catppuccin Macchiato.theme".source = "${catppuccin-fish}/themes/Catppuccin Macchiato.theme";
  programs.tmux = {
    enable = true;
    baseIndex = 1;
    extraConfig = ''
      setw -g pane-base-index 1
    '';
    shell = "${pkgs.fish}/bin/fish";
    mouse = true;
    escapeTime = 10;
  };

  # Not great now, perhaps later?
  # Plugins are kinda annoying
  #programs.neovim = {
  #  enable = true;
  #  defaultEditor = true;
  #  viAlias = true;
  #  vimAlias = true;
  #  vimdiffAlias = true;
  #  extraLuaConfig = ''
  #    vim.g.mapleader = " "
  #    vim.opt.scrolloff = 10
  #    vim.opt.colorcolumn = "140"
  #    vim.cmd("set number relativenumber")
  #  '';
  #  plugins = with pkgs.vimPlugins; [
  #    lazy-nvim {
  #      plugin = lazy-nvim;
  #    }
  #    conform-nvim {
  #    	plugin = conform-nvim;
  #      config = ''
  #      '';
  #    }
  #  ];
  #};
}


