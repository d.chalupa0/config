#!/bin/bash

# Set default values
dry_run=false
operation=""

# Parse command-line options
while [[ $# -gt 0 ]]; do
	case "$1" in
	--backup)
		operation="backup"
		;;
	--install)
		operation="install"
		;;
	-n)
		dry_run=true
		;;
	*)
		echo "Invalid option: $1"
		echo "Usage: $0 --backup (to copy) or --install (to restore) [-n] (optional flag for dry run)"
		exit 1
		;;
	esac
	shift
done

copy_with_dry_run() {
	source_path=$1
	dest_path=$2

	if [ "$dry_run" = true ]; then
		echo "Dry run: $source_path to $dest_path"
	else
		cp -r "$source_path" "$dest_path"
		echo "$source_path to $dest_path"
	fi
}

if [ "$operation" == "backup" ] || [ "$operation" == "install" ]; then
	# Create destination directories if they don't exist
	mkdir -p ./tmux
	mkdir -p ./nvim
	mkdir -p ./nix/home-manager

	# Define source and destination paths
	tmux_conf_source=~/.tmux.conf
	nvim_source=~/.config/nvim
	nix_source=~/.config/home-manager/home.nix

	tmux_dest=./tmux/.tmux.conf
	nvim_dest=./nvim
	nix_dest=./nix/home-manager/home.nix

	if [ "$operation" == "backup" ]; then
		copy_with_dry_run "$tmux_conf_source" "$tmux_dest"
		copy_with_dry_run "$nvim_source/." "$nvim_dest"
		copy_with_dry_run "$nix_source" "$nix_dest"
	elif [ "$operation" == "install" ]; then
		copy_with_dry_run "$tmux_dest" "$tmux_conf_source"
		copy_with_dry_run "$nvim_dest/." "$nvim_source"
		copy_with_dry_run "$nix_dest" "$nix_source"
	fi
	echo "$operation completed successfully."
else
	echo "Invalid operation. Use --backup to copy or --install to restore."
	exit 1
fi
