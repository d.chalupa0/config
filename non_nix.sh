# nvim install
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
chmod u+x nvim.appimage
./nvim.appimage

./nvim.appimage --appimage-extract
./squashfs-root/AppRun --version

## Optional: exposing nvim globally.
sudo mv squashfs-root /
sudo ln -s /squashfs-root/AppRun /usr/bin/nvim

# Rust install
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# rg, fd, bat
cargo install ripgrep
cargo install fd-find
cargo install --locked bat

# Fish shell (until full rust rewrite)
yum install cmake
yum install ncurses-devel

curl -LO https://github.com/fish-shell/fish-shell/releases/download/3.6.1/fish-3.6.1.tar.xz #or the latest
tar -xf fish-3.6.1.tar.xz
cd fish-3.6.1
cmake -DFISH_USE_SYSTEM_PCRE2=ON -DSYS_PCRE2_LIB=/usr/lib64/libpcre2-16.so .
make
sudo make install

# fisher
curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source && fisher install jorgebucaran/fisher

fisher install catppuccin/fish
fish_config theme save "Catppuccin Macchiato"

# fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
fisher install PatrickF1/fzf.fish
