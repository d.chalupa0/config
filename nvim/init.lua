-- bootstrap lazy.nvim, LazyVim and your plugins
require("config.lazy")
vim.opt.clipboard = "unnamedplus"
vim.cmd.colorscheme("catppuccin")
vim.opt.scrolloff = 10
vim.opt.colorcolumn = "140"
