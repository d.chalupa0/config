return {
   require("conform").setup({
      format_on_save = function(bufnr)
         if vim.g.disable_autoformat or vim.b[bufnr].disable_autoformat then
            return
         end
         return { timeout_ms = 500, lsp_fallback = true }
      end,
   }),

   vim.api.nvim_create_user_command("FormatDisable", function(args)
      if args.bang then
         vim.b.disable_autoformat = true
         vim.b.autoformat = false
      else
         vim.g.disable_autoformat = true
         vim.g.autoformat = false
      end
   end, {
      desc = "Disable autoformat-on-save",
      bang = true,
   }),

   vim.api.nvim_create_user_command("FormatEnable", function(args)
      if args.bang then
         vim.b.disable_autoformat = false
         vim.b.autoformat = true
      else
         vim.g.disable_autoformat = false
         vim.g.autoformat = true
      end
   end, {
      desc = "Enable autoformat-on-save",
      bang = true,
   }),
}
