return {
  "echasnovski/mini.indentscope",
  opts = {
    draw = {
      delay = 1,
      animation = require("mini.indentscope").gen_animation.none(),
    },
  },
}
